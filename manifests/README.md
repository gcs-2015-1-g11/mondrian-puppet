# MondrianPuppet

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with MondrianPuppet](#setup)
    * [What MondrianPuppet affects](#what-MondrianPuppet-affects)
    * [Beginning with MondrianPuppet](#beginning-with-MondrianPuppet)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

This Puppet module automates development environment setup for Pentaho Mondrian.

## Module Description

The module is used to install the dependencies needed for the environment setup for
those who seek a quick and easy solution to start collaborating with Pentaho Mondrian
project.

## Setup

### What MondrianPuppet affects

The module creates several directories for different versions of Java Development Kit, those
being jdk1.5, jdk1.6 and jdk1.7.
It also sets a group of environment variables in the machine necessary to build the Pentaho
Mondrian project.

### Beginning with MondrianPuppet

The only thing needed to start using this module is to apply the default manifest with
puppet apply. This will initialize the process of seting up the environment, which may take
a while to finish.

## Reference

These are the modules that are used by Mondrian Puppet:
- puppet-ant
- java
- puppet-jdkoracle
- wget

## Limitations

This module is compatible with any Linux distribution operating system.

## Development

The repository for contributions can be found in https://gitlab.com/gcs-2015-1-g11/mondrian-puppet.

## Release Notes/Contributors/Etc

This module was created as a project at Universidade de Brasilia.

Contributors to this module:
- Pedro Tomioka
- Lucas dos Santos
