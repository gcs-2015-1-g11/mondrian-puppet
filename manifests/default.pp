import 'nodes/*'

exec { "apt-get update":
  path => "/usr/bin",
}

package { 'git':
	ensure => present,
}

package { 'alien':
	ensure => present,
}

package { 'tomcat7':
	ensure => present,
	require => Exec["apt-get update"],
}

package { 'vim':
	ensure => present,
	require => Exec["apt-get update"],
}

# Class: ::puppet_repo
#
#
class { '::puppet_repo':
	user => 'vagrant',
	sharedf  => 'repo'
}

# install oracle jdk 7

class { 'jdk_oracle': 
			version => 7}

# install oracle jdk 6

class { 'java':
  package => 'openjdk-6-jdk'
}

class { 'ant':
version => '1.9.4'
}

# Class: ::puppet_path
#
#
class { '::puppet_path':
	user => 'vagrant'
}