# Class: puppet_repo
#
#
class puppet_repo ($user, $sharedf){
	

	$directory = "${user}/${sharedf}/mondrian"


	exec { 'Repository clone':
		command => "/usr/bin/git clone https://github.com/pentaho/mondrian.git",
		cwd => "/home/${user}/${sharedf}",
		creates => "/home/${directory}",
		logoutput => true,
		timeout => 0,
	}	
	notice('Cloning Repository. This may take a while...')

}